<?php

namespace Constants;

class MedicationConstants
{
    CONST GROUP = [ 
        "A" => "Group A - Medications/Supplements that the patient is currently taking at home that are NOT in the Hospital Discharge List.", 
        "B" => "Group B - Medications in the Discharge List that are NOT in the patient’s home, OR present, but patient had STOPPED taking it.", 
        "C" => "Group C – Medications in the Discharge List that does NOT MATCH the dose and/or frequency of the patient’s home medications" 
    ];

    CONST ATTACHMENTTYPE = [
        "PROFILE",
        "RECONCILIATION"
    ];
    
    CONST TYPE = [
        "soc" => "Start of Care",
        "rec" => "Recertification",
        "roc" => "Resumption of Care",
        "medication" => null
    ];
    
    CONST TITLE = [
        "soc" => "Medication Profile (" . self::TYPE["soc"] . ")",
        "rec" => "Medication Profile (" . self::TYPE["rec"] . ")",
        "roc" => "Medication Profile (" . self::TYPE["roc"] . ")",
        "medication" => "Medication Profile"
    ];

}
