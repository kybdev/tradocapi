<?php

namespace Constants;

class Insurance
{
    const MEDICARE = [
        15  => "Medicaid (HMO/Managed Care)",
        1   => "Medicaid Traditional",
        17  => "Medicare (Advantage/PFFS)",
        18  => "Medicare (HMO/Per Visit)",
        4   => "Medicare Traditional"
    ];

    const NONMEDICARE = [
        5   => "None - No Charge for Current Service",
        6   => "Other Government",
        7   => "Private HMO/Managed Care",
        8   => "Private Insurance",
        9   => "Self-Pay",
        10  => "Title Progams (e.g. Title III, V, or XX)",
        11  => "TRICARE",
        12  => "Unknown",
        13  => "Veteran Administration Plan",
        14  => "Worker's Compensation"
    ];
}
