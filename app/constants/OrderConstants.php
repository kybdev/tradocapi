<?php

namespace Constants;

class OrderConstants
{
    const CODE = [
        "adm",      //0
        "roc",      //1
        "rec",      //2
        "psyj",     //3
        "psy",      //4
        "dis",      //5
        "tra",      //6
        "f2f",      //7
        "poc-soc",  //8
        "poc-roc",  //9
        "poc-rec"   //10
    ];

    const POCCODE = [
        self::CODE[8],
        self::CODE[9],
        self::CODE[10],
    ];

    CONST DOCSTATCODE = [
        self::CODE[0]   => "ORDERS-ADM",
        self::CODE[1]   => "ORDERS-ROC",
        self::CODE[2]   => "ORDERS-REC",
        self::CODE[3]   => "ORDERS-PSYJ",
        self::CODE[4]   => "ORDERS-PSY",
        self::CODE[5]   => "ORDERS-DIS",
        self::CODE[6]   => "ORDERS-TRA",
        self::CODE[7]   => "ORDERS-F2F",
        self::CODE[8]   => "POC-SOC",
        self::CODE[9]   => "POC-ROC",
        self::CODE[10]  => "POC-REC"
    ];

    const MODELS = [
        self::CODE[0]   => "Models\Admission_order",
        self::CODE[1]   => "Models\Roc_order",
        self::CODE[2]   => "Models\Recert_order",
        self::CODE[3]   => "Models\Physicianorder_justification",
        self::CODE[4]   => "Models\Physicianorder",
        self::CODE[5]   => "Models\Discharge_order",
        self::CODE[6]   => "Models\Transfer_order",
        self::CODE[7]   => "Models\Face2face",
        self::CODE[8]   => "Models\Planofcare",
        self::CODE[9]   => "Models\Planofcare",
        self::CODE[10]  => "Models\Planofcare",
    ];

    const COMMUNICATIONMODELS = [
        self::CODE[0]   => "Models\Admission_order_communication_type",
        self::CODE[1]   => "Models\Roc_order_communication_type",
        self::CODE[2]   => "Models\Recert_order_communication_type",
        self::CODE[5]   => "Models\Discharge_order",
        self::CODE[6]   => "Models\Transfer_order_communication_type",
    ];

    const TYPE = [
        "Admission Order",
        "ROC Order",
        "REC Order",
        "Physician Order (Justification)",
        "Physician Order",
        "Discharge Order",
        "Transfer Report",
        "Physician Certification Face to Face Encounter",
        "Plan of Care/485 (SOC)",
        "Plan of Care/485 (ROC)",
        "Plan of Care/485 (REC)"
    ];

    const CODETYPE = [
        self::CODE[0]   => self::TYPE[0],
        self::CODE[1]   => self::TYPE[1],
        self::CODE[2]   => self::TYPE[2],
        self::CODE[3]   => self::TYPE[3],
        self::CODE[4]   => self::TYPE[4],
        self::CODE[5]   => self::TYPE[5],
        self::CODE[6]   => self::TYPE[6],
        self::CODE[7]   => self::TYPE[7],
        self::CODE[8]   => self::TYPE[8],
        self::CODE[9]   => self::TYPE[9],
        self::CODE[10]  => self::TYPE[10],
    ];

    const STATE = [
        self::CODE[0]   => "patientcare.admissionorder",
        self::CODE[1]   => "patientcare.rocorder",
        self::CODE[2]   => "patientcare.recertorder",
        self::CODE[3]   => "patientcare.physicianorderjustification",
        self::CODE[4]   => "patientcare.physicianorderedit",
        self::CODE[5]   => "patientcare.dischargeorder",
        self::CODE[6]   => "patientcare.transferorder",
        self::CODE[7]   => "patientcare.facetoface",
        self::CODE[8]   => "patientcare.planofcare",
        self::CODE[9]   => "patientcare.planofcare",
        self::CODE[10]  => "patientcare.planofcare",
    ];

    const STATUS = [
        "ADMITTED",
        "PENDING",
        "TRANSFERRED",
        "RECERTIFIED",
        "DISCHARGED",
        "NON-ADMIT"
    ];

    const FILTER = [
        [
            "code" => "all",
            "filter" => "All",
            "columns" => [
                [ "column" => "Order Date", "model" => "orderDate", "visible" => true, "width" => '110px' ],
                [ "column" => "Status", "model" => "status", "visible" => true, "width" => '110px' ],
                [ "column" => "Physician", "model" => "physician", "visible" => true, "width" => '110px' ],
                [ "column" => "Discipline", "model" => "discipline", "visible" => false, "width" => '100px' ],
                [ "column" => "Sent Date", "model" => "sentDate", "visible" => false ],
                [ "column" => "Received Date", "model" => "receiveDate", "visible" => false ],
            ]
        ],
        [
            "code" => "tbs",
            "filter" => "To be sent",
            "columns" => [
                [ "column" => "Order Date", "model" => "orderDate", "visible" => true, "width" => '110px' ],
                [ "column" => "Status", "model" => "status", "visible" => true, "width" => '110px' ],
                [ "column" => "Physician", "model" => "physician", "visible" => true, "width" => '110px' ],
                [ "column" => "Discipline", "model" => "discipline", "visible" => false, "width" => '100px' ],
                [ "column" => "Sent Date", "model" => "sentDate", "visible" => false, "width" => '95px' ],
                [ "column" => "Received Date", "model" => "receiveDate", "visible" => false, "width" => '95px' ],
            ]
        ],
        [
            "code" => "pps",
            "filter" => "Pending Physician Signature",
            "columns" => [
                [ "column" => "Order Date", "model" => "orderDate", "visible" => true, "width" => '110px' ],
                [ "column" => "Status", "model" => "status", "visible" => true, "width" => '110px' ],
                [ "column" => "Physician", "model" => "physician", "visible" => true, "width" => '110px' ],
                [ "column" => "Discipline", "model" => "discipline", "visible" => false, "width" => '100px' ],
                [ "column" => "Sent Date", "model" => "sentDate", "visible" => false, "width" => '95px' ],
                [ "column" => "Received Date", "model" => "receiveDate", "visible" => false, "width" => '95px' ],
            ],
            "sub" => [
                [ "code" => "all", "filter" => "All" ],
                [ "code" => "all-7-days", "filter" => "All > 7 Days" ],
                [ "code" => "all-30-days", "filter" => "All > 30 Days" ],
                [ "code" => "all-custom" ]
            ],
            "subActive" => "all",
            "allCustom" => null
        ],
        [
            "code" => "received",
            "filter" => "Received",
            "columns" => [
                [ "column" => "Order Date", "model" => "orderDate", "visible" => true, "width" => '110px' ],
                [ "column" => "Status", "model" => "status", "visible" => true, "width" => '110px' ],
                [ "column" => "Physician", "model" => "physician", "visible" => true, "width" => '110px' ],
                [ "column" => "Discipline", "model" => "discipline", "visible" => false, "width" => '100px' ],
                [ "column" => "Sent Date", "model" => "sentDate", "visible" => false, "width" => '95px' ],
                [ "column" => "Received Date", "model" => "receiveDate", "visible" => false, "width" => '95px' ],
            ]
        ]
    ];

    const ORDERSTATUS = [
        "IN PROCESS",
        "TO BE SENT",
        "PENDING PHYSICIAN SIGNATURE",
        "RECEIVED",
        "FOR QA",
        "REOPENED"
    ];
}
