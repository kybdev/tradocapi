<?php
/**
 * @author GEEKSNEST
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.

return [
    "prefix" => "/v1/example",
    "handler" => 'Controllers\ExampleController',
    "lazy" => true,
    "collection" => [
        [
            'method' => 'get',
            'route' => '/ping',
            'function' => 'pingAction',
            'authentication' => FALSE,
            'resource' => 'rl1' // OPTIONAL if Authentication is True
        ]
    ]
];

 */
return [
    "prefix" => "/v1/example",
    "handler" => 'Controllers\ExampleController',
    "lazy" => true,
    "collection" => [
        [
            'method' => 'post',
            'route' => '/samplenewnew',
            'function' => 'newnewsample',
            'authentication' => FALSE,
            'resource' => 'rl1'
        ],
        [
            'method' => 'get',
            'route' => '/samplenewnew',
            'function' => 'getsample',
            'authentication' => FALSE,
            'resource' => 'rl1'
        ],
        [
            'method' => 'put',
            'route' => '/samplenewnew',
            'function' => 'putsample',
            'authentication' => FALSE,
            'resource' => 'rl1'
        ],
        [
            'method' => 'delete',
            'route' => '/samplenewnew',
            'function' => 'deletesample',
            'authentication' => FALSE,
            'resource' => 'rl1'
        ],
        [
            'method' => 'get',
            'route' => '/ping',
            'function' => 'pingAction',
            'authentication' => FALSE,
            'resource' => 'rl1'
        ],
        [
            'method' => 'post',
            'route' => '/test/{id}',
            'function' => 'testAction',
            'authentication' => FALSE,
            'resource' => 'rl1'
        ],
        [
            'method' => 'post',
            'route' => '/auth/test/{name}',
            'function' => 'testAuth',
            'authentication' => TRUE,
            'resource' => 'rl1'
        ],
        [
            'method' => 'post',
            'route' => '/img',
            'function' => 'example_img_to_base64',
            'authentication' => TRUE,
            'resource' => 'rl1'
        ],
        [
            'method' => 'get',
            'route' => '/img/get',
            'function' => 'exampleget',
            'authentication' => false
        ],
        [
            'method' => 'get',
            'route' => '/redis/test',
            'function' => 'redisTest',
            'authentication' => true,
            'resource' => 'sys-000'

        ],
        [
            'method' => 'get',
            'route' => '/filldocumentid',
            'function' => 'filldocumentid',
            'authentication' => false,
            'resource' => 'sys-000'

        ],
        [
            'method' => 'get',
            'route' => '/amazontest',
            'function' => 'amazontest',
            'authentication' => false,
            'resource' => 'sys-000'

        ],
        [
            'method' => 'post',
            'route' => '/ocr',
            'function' => 'ocr',
            'authentication' => false,
            'resource' => 'sys-000'

        ],
        [
            'method' => 'get',
            'route' => '/sampleqr',
            'function' => 'sampleqr',
            'authentication' => false,
            'resource' => 'sys-000'

        ]
    ]
];
