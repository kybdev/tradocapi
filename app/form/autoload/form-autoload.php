<?php

 // LISTING OF FOLDER DIRECTORY
 // NOTE: Ill finish this later - kyben
function arrayDir($dir, $cDir){
	foreach (scandir($dir.$cDir) as $key => $value) {
		// echo $value;
	}
 }

 arrayDir($dir, "/form/module");



$autoloadForm = [
	'Form\Heirarchy' => $dir . '/form/autoload',

	//MODEL
	'Form\Model' => $dir . '/form/model/',

	//Static
	'Form\Stat' => array(
		$dir . '/form/static/oasis/oasisClinical',
	),

	//INPUTS
	'Form\Inputs' => $dir . '/form/html/inputs',

	//TEMPLATE
	'Form\Template' => array(
		$dir . '/form/html/template',
	),

	// MODULES
	'Form\Module' => array(
		$dir . '/form/module/oasis/oasisClinical',
		$dir . '/form/module/oasis/oasisDiagnosis',
	),


];

return $autoloadForm;
