<?php

namespace Controllers;

use Controllers\ControllerBase as CB;
use Controllers\EmailController as EM;
use Models\Patient;
use Security\AclRoles;
use S3;
use Phalcon\Crypt;
use Notes\Model\Notes;
use Models\Patientepisodetask;
use Controllers\AmazonController as AC;
use TesseractOCR;
use QrReader;

class ExampleController extends ControllerBase{


    public function newnewsample(){

        // Create an instance
        $crypt = new Crypt();

        $key  = "This is a secret key (32 bytes).";
        $text = "This is the text that you want to encrypt.";

        $encrypted = $crypt->encryptBase64($text, $key);

        return array('0' => $crypt->decryptBase64($encrypted, $key));
        // var_dump("POST");
        // die();
    }

    public function getsample(){
        var_dump("GET");
        die();
    }

    public function putsample(){
        var_dump("PUT");
        die();
    }

    public function deletesample(){
        var_dump("delete");
        die();
    }


	public function pingAction() {
		// $S3 = CB::S3initalize();
		// $request = new \Phalcon\Http\Request();
		// $file   = $request->getPost('file');

		// $getobject = S3::putObject('', $S3->bucket, 'uploads/memberfiles/asdsad/', S3::ACL_PUBLIC_READ);
		// $getobject = S3::getObject($S3->bucket, 'uploads/memberfiles/dbhcare.sql');
		// $getobject = S3::copyObject($S3->bucket, 'uploads/memberfiles/44B11111-0A6F-422B-B323-1CBF7751B9F2/ggggggggg/zxccccccccccccc/', $S3->bucket, 'uploads/memberfiles/rain/ggggggggg/zxccccccccccccc/', $metaHeaders = array(), $requestHeaders = array());
		// var_dump($getobject->headers['type']);
		// header('Content-Description: File Transfer');
    	//this assumes content type is set when uploading the file.
		// header('Content-Type: ' . $getobject->headers['type']);
		// header('Content-Disposition: attachment; filename=' . 'dbhcare.sql');
		// header('Expires: 0');
		// header('Cache-Control: must-revalidate');
		// header('Pragma: public');
        // throw new \Micro\Exceptions\HTTPExceptions(
        //     "this is an error",
        //     405,
        //     array(
        //         'dev' => "adsfadsfadsf",
        //         'internalCode' => "EXC001",
        //         'more' => "aSDasd"
        //     )
        // );

        return array('asdasd'=>'asd');
		// echo $getobject->body;
		// if($getobject){
		// 	echo $S3->bucket;
		// }
		// else{
		// 	echo 'false';
		// }

//        $emailcontent['username'] = 'hartjo';
//        $emailcontent['password'] = CB::randomPassword();
//        $emailcontent['token'] = CB::randomPassword();
//        $email = EM::mailTemplate($emailcontent);
//        echo $email;

        // throw new \Micro\Exceptions\HTTPExceptions(
        //     "this is an error",
        //     406,
        //     array(
        //         'dev' => "adsfadsfadsf",
        //         'internalCode' => "EXC002",
        //         'more' => "aSDasd"
        //     )
        // );

        // return $data;

	}

    public function testAuth($name){
        $data = array($name,'ResponseCode' => 202);
        return $data;
    }

    public function testAction($id) {
        echo "test (id: $id)";
    }

    public function skipAction($name) {
        echo "auth skipped ($name)";
    }

    public function samplequery(){

        // $sample = CB::createBuilder()
        // ->columns(array('Models\Membersdirectory.directoryid','Models\Membersfile.filepath'))
        // ->from('Models\Membersdirectory')
        // ->leftJoin('Models\Membersfile', 'Models\Membersdirectory.contentpath = Models\Membersfile.filepath')
        // ->where('Models\Membersdirectory.directoryid = "8CE049B1-3D42-4FF5-A121-98AA3EEBECE7"')
        // ->getQuery()
        // ->execute();

        // $sample = CB::createBuilder()
        // ->from('Models\Membersdirectory')
        // ->getQuery()
        // ->execute();

        // $sample = CB::customQuery('SELECT * FROM membersdirectory');

        // echo json_encode($sample->toArray());
    }

    public function examplepost() {

    }

    public function exampleget() {

    }

    public function redisTest(){
        //return [$this->checkRedis()];
        // return [$this->redisWrapper->redisSAdd("hc_require_relogin", 'efren') , $this->redisWrapper->redisSMembers('hc_require_relogin')];
        // return [$this->redisSRem("hc_require_relogin", 'efren') , $this->redisSMembers('hc_require_relogin')];
        // return [$this->redisWrapper->checkRedis()];
        return [$this->redisWrapper->redisSMembers('hc_require_relogin')];
    }

    public function filldocumentid(){


        $findnotes = Notes::find();

        if($findnotes){
            foreach ($findnotes as $note) {
                $findtask = Patientepisodetask::findFirst("taskid = '".$note->referenceid."'");

                if($findtask){
                    $findtask->documentid = $note->notesid;

                    if($findtask->save()){

                    }
                }
            }
        }



        $findtaskoasis = Patientepisodetask::find("idoasis != ''");

        if($findtaskoasis){
            foreach ($findtaskoasis as $task) {
                $findtask = Patientepisodetask::findFirst("idoasis = '".$task->idoasis."'");

                if($findtask){
                    $findtask->documentid = $task->idoasis;

                    if($findtask->save()){

                    }
                }
            }
        }


    }

    public function amazontest(){

        $ac = new AC();

        var_dump($ac->filesACL());

        // return $ac->filesACL();

        // $key = 'testfile2.jpg';
        // $presignedurl = $ac->s3PreSignUrl($key, 'haha.jpg', false);

        // return array("url" => $presignedurl);
    }

    public function ocr(){

        $dir = dirname(__DIR__);

        $filename = $_FILES['file']['name'];

        $file = file_get_contents('https://medisource.s3.amazonaws.com/uploads/patients/029BC87F-2A58-46C0-ACEE-1DF76483F25E/md_orders/6hegObXnToQiPdHTyo5lgmoZhwYRq4Hg.jpg');

        move_uploaded_file( $file ,'temp/'.$filename);


        $file = 'https://medisource.s3.amazonaws.com/uploads/patients/029BC87F-2A58-46C0-ACEE-1DF76483F25E/md_orders/6hegObXnToQiPdHTyo5lgmoZhwYRq4Hg.jpg';
        $newfile = $_SERVER['DOCUMENT_ROOT'] . '/temp/yoyo.jpg';

        if ( copy($file, $newfile) ) {
            echo "Copy success!";
        }else{
            echo "Copy failed.";
        }

        // $file = file_get_contents('https://medisource.s3.amazonaws.com/uploads/patients/029BC87F-2A58-46C0-ACEE-1DF76483F25E/md_orders/6hegObXnToQiPdHTyo5lgmoZhwYRq4Hg.jpg');

        $sample =  (new TesseractOCR('temp/'. $filename))
                ->run();

        return ['sample' => $sample];
    }

    public function sampleqr(){

        $qrcode = new QrReader('temp/qr.jpg');

        return ["qr" => '$qrcode'];

    }

}
