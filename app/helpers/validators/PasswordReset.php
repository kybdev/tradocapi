<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;
use Models\Members;

class PasswordReset extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("pin", new PresenceOf(["message" => "Pin is required.",]));
        $this->add("password", new PresenceOf(["message" => "Password is required.",]));
        $this->add("repassword", new PresenceOf(["message" => "Confirm Password is required.",]));
    }

}
