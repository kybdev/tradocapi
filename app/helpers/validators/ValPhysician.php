<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

use Models\Physician;

class ValPhysician extends Validation
{
    public function initialize()
    {
        // Checking that must be required
        $this->add("npi", new PresenceOf(["message" => "NPI is required",]));
        $this->add("firstName", new PresenceOf(["message" => "First Name is required",]));
        $this->add("lastName", new PresenceOf(["message" => "Last Name is required",]));
        // $this->add("midleName", new PresenceOf(["message" => "Middle Name is required",]));
        // $this->add("address", new PresenceOf(["message" => "Address One is required",]));
        // $this->add("city", new PresenceOf(["message" => "City is required",]));
        // $this->add("zipCode", new PresenceOf(["message" => "Zip Code is required",]));
        // $this->add("licensed", new PresenceOf(["message" => "Licensed # is required",]));
        // $this->add("expiration", new PresenceOf(["message" => "Expiration is required",]));
        // $this->add("licensedState", new PresenceOf(["message" => "Licensed State is required",]));
        // $this->add("phycode", new PresenceOf(["message" => "Physician Code is required",]));
        // $this->add("credentials", new PresenceOf(["message" => "Credential is required",]));
        // $this->add("phone", new PresenceOf(["message" => "Phone is required",]));
        // $this->add("email", new PresenceOf(["message" => "E-mail is required",]));
        // $this->add("protocol", new PresenceOf(["message" => "Protocol is required",]));

        //Email validity
        // $this->add("email", new Email(["message" => "The e-mail is not valid"]));

        //Expiration Date
        // $this->add("expiration", new Date(["format" => "Y-m-d","message" => "The Expiration is not a valid date or format"]));

    }
}
