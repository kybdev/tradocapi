<?php

namespace RequestValidator\Custom;
use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;

class Uniquedata extends Validator
{
    /**
    * Executes the validation
    *
    * @param Phalcon\Validation $validator
    * @param string $attribute
    * @return boolean
    */
    public function validate(Validation $validator, $attribute)
    {

        $value = $validator->getValue($attribute);
        $condition = $attribute . " = ?0 ";
        $bind = [$value];

        $exclude = $this->getOption("exclude");

        if(is_array($exclude)){
            $message = $this->getOption("message");
            $condition .= " AND (".$exclude['con'].")";
            foreach ($exclude['bind'] as $value) {
                $bind[] = $value;
            }
        }else{
            $message = $this->getOption("message");
        }

        $model = $this->getOption("model");

        $data = $model::findFirst([
            $condition,
            "bind" => $bind
        ]);
        if($data){
            $validator->appendMessage(new Message($message, $attribute, "Uniquememberdata"));
            return true;
        }
        return true;
    }
}
