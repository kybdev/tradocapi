<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;

class Patient_medications extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("fill_date", new PresenceOf(["message" => "Date ordered is required",]));
        $this->add("grouplegend", new PresenceOf(["message" => "Group legend is required",]));
        $this->add("code", new PresenceOf(["message" => "Code is required",]));
        $this->add("medication", new PresenceOf(["message" => "Medication is required",]));
        // $this->add("amount", new PresenceOf(["message" => "Amount is required",]));
        // $this->add("route", new PresenceOf(["message" => "Route of administration is required",]));
        // $this->add("frequency", new PresenceOf(["message" => "Frequency is required",]));
        $this->add("classification", new PresenceOf(["message" => "Indication/Classification is required",]));
    }

}