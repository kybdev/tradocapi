<?php
namespace Micro\Responses;

class Response extends \Phalcon\DI\Injectable{

    protected $head = false;

    public function __construct(){
        //parent::__construct();
        $di = \Phalcon\DI::getDefault();
        $this->setDI($di);
    }

    /**
     * In-Place, recursive conversion of array keys in snake_Case to camelCase
     * @param  array $snakeArray Array with snake_keys
     * @return  no return value, array is edited in place
     */
    public function convertKeysToCamelCase($param)
    {
        $arr = [];
        foreach ($param as $key => $value) {
            $key = preg_replace_callback(
                '/_([^_])/',
                function (array $m) {
                    return ucfirst($m[1]);
                },
                $key
            );

            if (is_array($value))
                $value = $this->convertKeysToCamelCase($value);

            $arr[$key] = $value;
        }
        return $arr;
    }
}