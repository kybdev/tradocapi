<?php

namespace Template\Model;


class TemplateModel {

    public static function modelDefault($model){

        return '<?php
            namespace Models;

            class '.$model.' extends Basemodel {

                public function initialize() {

                }

            }';
    }
}
