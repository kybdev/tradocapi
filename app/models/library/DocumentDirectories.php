<?php

namespace LibraryModels;

use Models\Basemodel;

class DocumentDirectories extends Basemodel
{
	public function initialize() {
		$this->table = 'document_directories';
	}
}